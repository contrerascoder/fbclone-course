# FBClone
Esta app va a ser una app para enseñar a hacer apps con nodejs y va a consistir en una red social similar a la de facebook

Las etapas de la creación de la app van a ser:
 - Configuración de la app y primera ruta (VV)
 - Configuración de la conexión a la base de datos (VV)
 - Creación de el modelo User (VV)
 - Creación de la aplicación de Nuxt (VV)
 - Configuración de axios y primera petición (VV)
 - Rutas de registro y de login (VV)
 - Web tokens con jsonwebtoken (VV)
 - Encriptando contraseña (VV)
 - Validación de campos (VV)
 - Creando un cr (create, read) de microposts (VV)
 - Haciendo sistema de comentarios para los microposts (parte 1 - Create, Read) (VV)
 - Haciendo sistema de comentarios para los microposts (parte 2 - Update, Delete) (VV)
 - Comentarios anidados (VV)
 - Pagina personal - Listado de amigos y poder eliminarlos (VV)
 - Compilando app y deseplegando app a heroku (VV)
 - Recuperación de contraseña (v)
 - Verificar email (V)
 - Subir ímagenes al servidor (V)
 - Segundo deployment

> Para las secciones que se haya completado el commit se marcara con una V, para las secciones que se hayan documentado ya se marcará con una doble V (VV)


 - Cambiar títulos (V)
 - Actualizar css (V)
 - Presentar el curso (requerimientos para el curso)
 - Compilar y empaquetar en electron
