"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = exports.authController = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _User = _interopRequireDefault(require("../../models/User"));

var _jwt = require("../../utils/jwt");

var _crypt = require("../../utils/crypt");

var _uniqueString = _interopRequireDefault(require("unique-string"));

var _vars = require("../../config/vars");

var _passwordMailer = _interopRequireDefault(require("../../utils/mailer/password-mailer"));

var _verifyMailer = _interopRequireDefault(require("../../utils/mailer/verify-mailer"));

var _fsExtra = _interopRequireDefault(require("fs-extra"));

var _path = _interopRequireDefault(require("path"));

var AuthController = /*#__PURE__*/function () {
  function AuthController() {
    (0, _classCallCheck2["default"])(this, AuthController);
  }

  (0, _createClass2["default"])(AuthController, [{
    key: "signup",
    value: function () {
      var _signup = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee(req, res) {
        var user, userCreated, mailer;
        return _regenerator["default"].wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.next = 2;
                return _User["default"].findOne({
                  email: req.body.email
                });

              case 2:
                user = _context.sent;

                if (!user) {
                  _context.next = 5;
                  break;
                }

                return _context.abrupt("return", res.status(400).end('El usuario existe'));

              case 5:
                _context.next = 7;
                return _User["default"].create(req.body);

              case 7:
                userCreated = _context.sent;
                mailer = new _verifyMailer["default"](userCreated.email);
                _context.next = 11;
                return mailer.buildAndSend({
                  link: "".concat(_vars.APP_BASE_URL, "/auth/users/").concat(userCreated.secret, "/activate")
                });

              case 11:
                res.status(201).json(userCreated);

              case 12:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }));

      function signup(_x, _x2) {
        return _signup.apply(this, arguments);
      }

      return signup;
    }()
  }, {
    key: "signin",
    value: function () {
      var _signin = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee2(req, res) {
        var user, tokenData, token;
        return _regenerator["default"].wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.next = 2;
                return _User["default"].findOne({
                  email: req.body.email,
                  verified: true
                });

              case 2:
                user = _context2.sent;

                if (user) {
                  _context2.next = 5;
                  break;
                }

                return _context2.abrupt("return", res.status(400).end('Las credenciales son incorrectas'));

              case 5:
                _context2.next = 7;
                return (0, _crypt.checkPassword)(req.body.password, user.password);

              case 7:
                if (_context2.sent) {
                  _context2.next = 9;
                  break;
                }

                return _context2.abrupt("return", res.status(400).end('Las credenciales son incorrectas'));

              case 9:
                tokenData = {
                  name: user.name,
                  email: user.email,
                  avatar: user.avatar
                };
                token = (0, _jwt.tokenize)(tokenData);
                return _context2.abrupt("return", res.status(201).json({
                  token: token,
                  userData: tokenData
                }));

              case 12:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2);
      }));

      function signin(_x3, _x4) {
        return _signin.apply(this, arguments);
      }

      return signin;
    }()
  }, {
    key: "sendPasswordLink",
    value: function () {
      var _sendPasswordLink = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee3(req, res) {
        var user, mailer;
        return _regenerator["default"].wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                _context3.next = 2;
                return _User["default"].findOne({
                  email: req.query.email
                });

              case 2:
                user = _context3.sent;
                console.log(user);
                mailer = new _passwordMailer["default"](user.email);
                _context3.next = 7;
                return mailer.buildAndSend({
                  link: "".concat(_vars.APP_BASE_URL, "/auth/users/").concat(user.secret, "/pass"),
                  fullname: 'raul contreras'
                });

              case 7:
                res.status(200).end('email enviado');

              case 8:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3);
      }));

      function sendPasswordLink(_x5, _x6) {
        return _sendPasswordLink.apply(this, arguments);
      }

      return sendPasswordLink;
    }()
  }, {
    key: "changePassword",
    value: function () {
      var _changePassword = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee4(req, res) {
        var newPass;
        return _regenerator["default"].wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                _context4.next = 2;
                return (0, _crypt.hashPassword)(req.body.password);

              case 2:
                newPass = _context4.sent;
                console.log(newPass);
                _context4.next = 6;
                return _User["default"].updateOne({
                  secret: req.params.secret
                }, {
                  $set: {
                    password: newPass
                  }
                });

              case 6:
                _context4.next = 8;
                return updateAndGetUniqueString();

              case 8:
                res.status(200).end('Contraseña cambiada');

              case 9:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4);
      }));

      function changePassword(_x7, _x8) {
        return _changePassword.apply(this, arguments);
      }

      return changePassword;
    }()
  }, {
    key: "activate",
    value: function () {
      var _activate = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee5(req, res) {
        return _regenerator["default"].wrap(function _callee5$(_context5) {
          while (1) {
            switch (_context5.prev = _context5.next) {
              case 0:
                _context5.next = 2;
                return _User["default"].findOneAndUpdate({
                  secret: req.params.secret
                }, {
                  $set: {
                    verified: true
                  }
                });

              case 2:
                res.status(200).end('Cuenta activada');

              case 3:
              case "end":
                return _context5.stop();
            }
          }
        }, _callee5);
      }));

      function activate(_x9, _x10) {
        return _activate.apply(this, arguments);
      }

      return activate;
    }()
  }, {
    key: "updateAvatar",
    value: function () {
      var _updateAvatar = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee6(req, res) {
        var newName, addressAvatar;
        return _regenerator["default"].wrap(function _callee6$(_context6) {
          while (1) {
            switch (_context6.prev = _context6.next) {
              case 0:
                newName = req.user._id + ".png";
                _context6.next = 3;
                return _fsExtra["default"].rename(req.file.path, _path["default"].join(req.file.destination, newName));

              case 3:
                addressAvatar = _vars.APP_BASE_FILES_ADDRESS + "/img/avatars/" + newName;

                _User["default"].updateOne({
                  _id: req.user._id
                }, {
                  $set: {
                    avatar: addressAvatar
                  }
                });

                res.status(200).end(addressAvatar);

              case 6:
              case "end":
                return _context6.stop();
            }
          }
        }, _callee6);
      }));

      function updateAvatar(_x11, _x12) {
        return _updateAvatar.apply(this, arguments);
      }

      return updateAvatar;
    }()
  }]);
  return AuthController;
}();

function updateAndGetUniqueString(_x13) {
  return _updateAndGetUniqueString.apply(this, arguments);
}

function _updateAndGetUniqueString() {
  _updateAndGetUniqueString = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee7(email) {
    var newSecret, data;
    return _regenerator["default"].wrap(function _callee7$(_context7) {
      while (1) {
        switch (_context7.prev = _context7.next) {
          case 0:
            newSecret = (0, _uniqueString["default"])();
            data = {
              $set: {
                secret: newSecret
              }
            };
            _context7.next = 4;
            return _User["default"].findOneAndUpdate({
              email: email
            }, {
              $set: data
            });

          case 4:
            return _context7.abrupt("return", newSecret);

          case 5:
          case "end":
            return _context7.stop();
        }
      }
    }, _callee7);
  }));
  return _updateAndGetUniqueString.apply(this, arguments);
}

var authController = new AuthController();
exports.authController = authController;
var _default = authController;
exports["default"] = _default;