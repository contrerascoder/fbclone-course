"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = exports.authRouter = void 0;

var _express = _interopRequireDefault(require("express"));

var _AuthController = _interopRequireDefault(require("./AuthController"));

var _validators = require("../../utils/validators");

var _diskStorage = _interopRequireDefault(require("../../utils/disk-storage"));

var uploader = (0, _diskStorage["default"])('img/avatars');

var authRouter = _express["default"].Router();

exports.authRouter = authRouter;
authRouter.post('/signup', _validators.signUpValidator, _AuthController["default"].signup);
authRouter.post('/signin', _validators.signInValidator, _AuthController["default"].signin);
authRouter.get('/passlink', _AuthController["default"].sendPasswordLink);
authRouter.put('/password/:secret', _AuthController["default"].changePassword);
authRouter.put('/verify/:secret', _AuthController["default"].activate);
authRouter.put('/avatar', uploader.single('avatar'), _AuthController["default"].updateAvatar);
var _default = authRouter;
exports["default"] = _default;