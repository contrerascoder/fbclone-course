"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = exports.friendsController = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _Friend = _interopRequireDefault(require("../../models/Friend"));

var _User = _interopRequireDefault(require("../../models/User"));

var FriendsController = /*#__PURE__*/function () {
  function FriendsController() {
    (0, _classCallCheck2["default"])(this, FriendsController);
  }

  (0, _createClass2["default"])(FriendsController, [{
    key: "getAll",
    value: function () {
      var _getAll = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee(req, res) {
        var people;
        return _regenerator["default"].wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.next = 2;
                return _Friend["default"].getOtherPeople(req.user._id);

              case 2:
                people = _context.sent;
                res.status(200).json({
                  people: people
                });

              case 4:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }));

      function getAll(_x, _x2) {
        return _getAll.apply(this, arguments);
      }

      return getAll;
    }()
  }, {
    key: "sendRequest",
    value: function () {
      var _sendRequest = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee2(req, res) {
        return _regenerator["default"].wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.next = 2;
                return _Friend["default"].createRequest(req.user._id, req.params.id);

              case 2:
                res.status(201).end('Petición mandada');

              case 3:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2);
      }));

      function sendRequest(_x3, _x4) {
        return _sendRequest.apply(this, arguments);
      }

      return sendRequest;
    }()
  }, {
    key: "getRequests",
    value: function () {
      var _getRequests = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee3(req, res) {
        var requests;
        return _regenerator["default"].wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                _context3.next = 2;
                return _Friend["default"].getPendingRequestsToUser(req.user._id);

              case 2:
                requests = _context3.sent;
                res.status(200).json({
                  requests: requests
                });

              case 4:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3);
      }));

      function getRequests(_x5, _x6) {
        return _getRequests.apply(this, arguments);
      }

      return getRequests;
    }()
  }, {
    key: "acceptRequest",
    value: function () {
      var _acceptRequest = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee4(req, res) {
        return _regenerator["default"].wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                _context4.next = 2;
                return _Friend["default"].acceptRequest(req.params.id);

              case 2:
                res.status(200).end('Petición aceptada');

              case 3:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4);
      }));

      function acceptRequest(_x7, _x8) {
        return _acceptRequest.apply(this, arguments);
      }

      return acceptRequest;
    }()
  }, {
    key: "getFriends",
    value: function () {
      var _getFriends = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee5(req, res) {
        var friends;
        return _regenerator["default"].wrap(function _callee5$(_context5) {
          while (1) {
            switch (_context5.prev = _context5.next) {
              case 0:
                _context5.next = 2;
                return _Friend["default"].getFriendsFrom(req.user._id);

              case 2:
                friends = _context5.sent;
                res.status(200).json({
                  friends: friends
                });

              case 4:
              case "end":
                return _context5.stop();
            }
          }
        }, _callee5);
      }));

      function getFriends(_x9, _x10) {
        return _getFriends.apply(this, arguments);
      }

      return getFriends;
    }()
  }, {
    key: "removeFriend",
    value: function () {
      var _removeFriend = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee6(req, res) {
        return _regenerator["default"].wrap(function _callee6$(_context6) {
          while (1) {
            switch (_context6.prev = _context6.next) {
              case 0:
                _context6.next = 2;
                return _Friend["default"].removeFriend(req.params.id, req.user._id);

              case 2:
                res.status(200).end('Amigo eliminado');

              case 3:
              case "end":
                return _context6.stop();
            }
          }
        }, _callee6);
      }));

      function removeFriend(_x11, _x12) {
        return _removeFriend.apply(this, arguments);
      }

      return removeFriend;
    }()
  }]);
  return FriendsController;
}();

var friendsController = new FriendsController();
exports.friendsController = friendsController;
var _default = friendsController;
exports["default"] = _default;