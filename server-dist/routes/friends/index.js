"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _express = _interopRequireDefault(require("express"));

var _FriendsController = _interopRequireDefault(require("./FriendsController"));

var friendsRouter = _express["default"].Router(); // Ruta para obtener todas las personas


friendsRouter.get('/', _FriendsController["default"].getAll); // Ruta para mandar peticiones

friendsRouter.post('/request/:id', _FriendsController["default"].sendRequest); // Ruta para obtener las peticiones

friendsRouter.get('/requests', _FriendsController["default"].getRequests); // Ruta para aceptar peticiones

friendsRouter.put('/accept/:id', _FriendsController["default"].acceptRequest); // Ruta para obtener todos los amigos

friendsRouter.get('/friends', _FriendsController["default"].getFriends); // Ruta para eliminar amigos

friendsRouter["delete"]('/remove/:id', _FriendsController["default"].removeFriend);
var _default = friendsRouter;
exports["default"] = _default;