"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _express = _interopRequireDefault(require("express"));

var _PostController = _interopRequireDefault(require("./PostController"));

var _isLogged = _interopRequireDefault(require("../../utils/is-logged"));

var _diskStorage = _interopRequireDefault(require("../../utils/disk-storage"));

var uploader = (0, _diskStorage["default"])('img/posts');

var postsRouter = _express["default"].Router();

postsRouter.post('/', _isLogged["default"], uploader.any(), _PostController["default"].create);
postsRouter.get('/', _PostController["default"].getAll);
postsRouter.put('/:postId', _isLogged["default"], _PostController["default"].update);
postsRouter["delete"]('/:postId', _isLogged["default"], _PostController["default"]["delete"]);
var _default = postsRouter;
exports["default"] = _default;