"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = exports.postController = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _fsExtra = _interopRequireDefault(require("fs-extra"));

var _vars = require("../../config/vars");

var _require = require("../../models/Post"),
    PostsModel = _require["default"];

var PostController = /*#__PURE__*/function () {
  function PostController() {
    (0, _classCallCheck2["default"])(this, PostController);
  }

  (0, _createClass2["default"])(PostController, [{
    key: "create",
    value: function () {
      var _create = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee(req, res) {
        var dataPost, image, post;
        return _regenerator["default"].wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                dataPost = {
                  text: req.body.message,
                  user: req.user
                };
                image = req.files && req.files[0];

                if (!image) {
                  _context.next = 6;
                  break;
                }

                _context.next = 5;
                return _fsExtra["default"].rename(image.path, image.path + ".png");

              case 5:
                dataPost.image = _vars.APP_BASE_FILES_ADDRESS + "/img/posts/" + image.filename + ".png";

              case 6:
                _context.next = 8;
                return PostsModel.create(dataPost);

              case 8:
                post = _context.sent;
                return _context.abrupt("return", res.status(201).json(post));

              case 10:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }));

      function create(_x, _x2) {
        return _create.apply(this, arguments);
      }

      return create;
    }()
  }, {
    key: "getAll",
    value: function () {
      var _getAll = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee2(req, res) {
        var posts;
        return _regenerator["default"].wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.next = 2;
                return PostsModel.find({}).sort('-date').populate('user', ['name', 'surname']);

              case 2:
                posts = _context2.sent;
                return _context2.abrupt("return", res.status(200).json(posts));

              case 4:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2);
      }));

      function getAll(_x3, _x4) {
        return _getAll.apply(this, arguments);
      }

      return getAll;
    }()
  }, {
    key: "update",
    value: function () {
      var _update = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee3(req, res) {
        var update;
        return _regenerator["default"].wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                update = {
                  text: req.body.message
                };
                _context3.next = 3;
                return PostsModel.updateOne({
                  _id: req.params.postId
                }, {
                  $set: update
                });

              case 3:
                _context3.t0 = res.status(200);
                _context3.next = 6;
                return PostsModel.findOne({
                  _id: req.params.postId
                }).populate('user', ['name', 'surname']);

              case 6:
                _context3.t1 = _context3.sent;
                _context3.t2 = {
                  post: _context3.t1
                };
                return _context3.abrupt("return", _context3.t0.json.call(_context3.t0, _context3.t2));

              case 9:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3);
      }));

      function update(_x5, _x6) {
        return _update.apply(this, arguments);
      }

      return update;
    }()
  }, {
    key: "delete",
    value: function () {
      var _delete2 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee4(req, res) {
        var post;
        return _regenerator["default"].wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                _context4.next = 2;
                return PostsModel.deleteOne({
                  _id: req.params.postId
                });

              case 2:
                post = _context4.sent;
                console.log(req.params.postId, post);
                return _context4.abrupt("return", res.status(200).end('Se elimino correctamente'));

              case 5:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4);
      }));

      function _delete(_x7, _x8) {
        return _delete2.apply(this, arguments);
      }

      return _delete;
    }()
  }]);
  return PostController;
}();

var postController = new PostController();
exports.postController = postController;
var _default = postController;
exports["default"] = _default;