"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = exports.commentController = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _Comment = _interopRequireDefault(require("../../models/Comment"));

var _Post = _interopRequireDefault(require("../../models/Post"));

var CommentController = /*#__PURE__*/function () {
  function CommentController() {
    (0, _classCallCheck2["default"])(this, CommentController);
  }

  (0, _createClass2["default"])(CommentController, [{
    key: "create",
    value: function () {
      var _create = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee(req, res) {
        var comment;
        return _regenerator["default"].wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.t0 = _Comment["default"];
                _context.t1 = req.body.message;
                _context.t2 = req.user;
                _context.next = 5;
                return _Post["default"].findOne({
                  _id: req.params.postId
                });

              case 5:
                _context.t3 = _context.sent;
                _context.t4 = {
                  text: _context.t1,
                  user: _context.t2,
                  post: _context.t3
                };
                _context.next = 9;
                return _context.t0.create.call(_context.t0, _context.t4);

              case 9:
                comment = _context.sent;
                return _context.abrupt("return", res.status(201).json(comment));

              case 11:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }));

      function create(_x, _x2) {
        return _create.apply(this, arguments);
      }

      return create;
    }()
  }, {
    key: "getAll",
    value: function () {
      var _getAll = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee2(req, res) {
        var comments;
        return _regenerator["default"].wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.next = 2;
                return _Comment["default"].find({
                  post: req.params.postId,
                  childOf: null
                }).sort('-date').populate('user', ['name', 'surname']);

              case 2:
                comments = _context2.sent;
                return _context2.abrupt("return", res.status(200).json(comments));

              case 4:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2);
      }));

      function getAll(_x3, _x4) {
        return _getAll.apply(this, arguments);
      }

      return getAll;
    }()
  }, {
    key: "respondToComment",
    value: function () {
      var _respondToComment = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee3(req, res) {
        var comment;
        return _regenerator["default"].wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                console.log(req.params.commentId);
                _context3.next = 3;
                return _Comment["default"].create({
                  text: req.body.message,
                  user: req.user,
                  // post: await PostsModel.findOne({_id: req.params.postId}),
                  childOf: req.params.commentId
                });

              case 3:
                comment = _context3.sent;
                return _context3.abrupt("return", res.status(201).json(comment));

              case 5:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3);
      }));

      function respondToComment(_x5, _x6) {
        return _respondToComment.apply(this, arguments);
      }

      return respondToComment;
    }()
  }, {
    key: "getResponses",
    value: function () {
      var _getResponses = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee4(req, res) {
        var comments;
        return _regenerator["default"].wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                _context4.next = 2;
                return _Comment["default"].find({
                  childOf: req.params.commentId
                }).sort('-date').populate('user', ['name', 'surname']);

              case 2:
                comments = _context4.sent;
                return _context4.abrupt("return", res.status(200).json(comments));

              case 4:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4);
      }));

      function getResponses(_x7, _x8) {
        return _getResponses.apply(this, arguments);
      }

      return getResponses;
    }()
  }]);
  return CommentController;
}();

var commentController = new CommentController();
exports.commentController = commentController;
var _default = commentController;
exports["default"] = _default;