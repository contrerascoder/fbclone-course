"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _express = _interopRequireDefault(require("express"));

var _CommentsController = _interopRequireDefault(require("./CommentsController"));

var _isLogged = _interopRequireDefault(require("../../utils/is-logged"));

var commentsRouter = _express["default"].Router();

commentsRouter.post('/post/:postId', _isLogged["default"], _CommentsController["default"].create);
commentsRouter.get('/post/:postId', _CommentsController["default"].getAll);
commentsRouter.post('/respond/:commentId', _isLogged["default"], _CommentsController["default"].respondToComment);
commentsRouter.get('/responses/:commentId', _CommentsController["default"].getResponses);
var _default = commentsRouter;
exports["default"] = _default;