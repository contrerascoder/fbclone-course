"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = exports.router = void 0;

var _express = _interopRequireDefault(require("express"));

var _auth = _interopRequireDefault(require("./auth"));

var _posts = _interopRequireDefault(require("./posts"));

var _comments = _interopRequireDefault(require("./comments"));

var _friends = _interopRequireDefault(require("./friends"));

var router = new _express["default"].Router();
exports.router = router;
router.use('/auth', _auth["default"]);
router.use('/posts', _posts["default"]);
router.use('/comments', _comments["default"]);
router.use('/friends', _friends["default"]);
var _default = router;
exports["default"] = _default;