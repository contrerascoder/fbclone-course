"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = exports.CommentsModel = exports.collectionCommentName = void 0;

var _mongoose = _interopRequireDefault(require("mongoose"));

var _User = require("./User");

var _Post = require("./Post");

var collectionCommentName = 'comments';
exports.collectionCommentName = collectionCommentName;
var schema = new _mongoose["default"].Schema({
  text: {
    type: String,
    "default": ''
  },
  date: {
    type: Number,
    "default": Date.now
  },
  user: {
    type: _mongoose["default"].Types.ObjectId,
    ref: _User.collectionUsersName
  },
  post: {
    type: _mongoose["default"].Types.ObjectId,
    ref: _Post.collectionPostsName
  },
  childOf: {
    type: _mongoose["default"].Types.ObjectId,
    ref: collectionCommentName,
    "default": null
  }
});

var CommentsModel = _mongoose["default"].model(collectionCommentName, schema);

exports.CommentsModel = CommentsModel;
var _default = CommentsModel;
exports["default"] = _default;