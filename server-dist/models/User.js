"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = exports.UserModel = exports.collectionUsersName = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _mongoose = _interopRequireDefault(require("mongoose"));

var _crypt = require("../utils/crypt");

var _uniqueString = _interopRequireDefault(require("unique-string"));

var collectionUsersName = 'users';
exports.collectionUsersName = collectionUsersName;
var schema = new _mongoose["default"].Schema({
  name: {
    type: String,
    required: true
  },
  surname: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  secret: {
    type: String,
    "default": _uniqueString["default"]
  },
  verified: {
    type: Boolean,
    "default": false
  },
  avatar: {
    type: String,
    "default": ''
  }
});
schema.pre('save', /*#__PURE__*/function () {
  var _ref = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee(next) {
    return _regenerator["default"].wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            console.log('encriptando contraseña', this.password);
            _context.next = 3;
            return (0, _crypt.hashPassword)(this.password);

          case 3:
            this.password = _context.sent;
            next();

          case 5:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, this);
  }));

  return function (_x) {
    return _ref.apply(this, arguments);
  };
}());

var UserModel = _mongoose["default"].model(collectionUsersName, schema);

exports.UserModel = UserModel;
var _default = UserModel;
exports["default"] = _default;