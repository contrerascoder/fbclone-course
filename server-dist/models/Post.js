"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = exports.PostsModel = exports.collectionPostsName = void 0;

var _mongoose = _interopRequireDefault(require("mongoose"));

var _crypt = require("../utils/crypt");

var _User = require("./User");

var collectionPostsName = 'posts';
exports.collectionPostsName = collectionPostsName;
var schema = new _mongoose["default"].Schema({
  text: {
    type: String,
    "default": ''
  },
  date: {
    type: Number,
    "default": Date.now
  },
  user: {
    type: _mongoose["default"].Types.ObjectId,
    ref: _User.collectionUsersName
  },
  image: {
    type: String,
    "default": ''
  }
});

var PostsModel = _mongoose["default"].model(collectionPostsName, schema);

exports.PostsModel = PostsModel;
var _default = PostsModel;
exports["default"] = _default;