"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = exports.friendsModel = exports.collectionFriendsName = void 0;

var _toConsumableArray2 = _interopRequireDefault(require("@babel/runtime/helpers/toConsumableArray"));

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _mongoose = _interopRequireDefault(require("mongoose"));

var _User = _interopRequireWildcard(require("./User"));

var _express = require("express");

var collectionFriendsName = 'friends';
exports.collectionFriendsName = collectionFriendsName;
var schema = new _mongoose["default"].Schema({
  from: {
    type: _mongoose["default"].Types.ObjectId,
    ref: _User.collectionUsersName
  },
  to: {
    type: _mongoose["default"].Types.ObjectId,
    ref: _User.collectionUsersName
  },
  status: {
    type: String,
    "enum": ['P', 'A'],
    "default": 'P'
  }
});

schema.statics.createRequest = function sendRequest(from, to) {
  return this.create({
    from: from,
    to: to
  });
};

schema.statics.getPendingRequestsFromUser = /*#__PURE__*/function () {
  var _getPendingRequestsFromUser = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee(user) {
    return _regenerator["default"].wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.next = 2;
            return this.find({
              from: user,
              status: 'P'
            });

          case 2:
            return _context.abrupt("return", _context.sent);

          case 3:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, this);
  }));

  function getPendingRequestsFromUser(_x) {
    return _getPendingRequestsFromUser.apply(this, arguments);
  }

  return getPendingRequestsFromUser;
}();

schema.statics.getPendingRequestsToUser = /*#__PURE__*/function () {
  var _getPendingRequestsToUser = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee2(user) {
    return _regenerator["default"].wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            _context2.next = 2;
            return this.find({
              to: user,
              status: 'P'
            }).populate('from', ['name', 'surname']);

          case 2:
            return _context2.abrupt("return", _context2.sent);

          case 3:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2, this);
  }));

  function getPendingRequestsToUser(_x2) {
    return _getPendingRequestsToUser.apply(this, arguments);
  }

  return getPendingRequestsToUser;
}();

schema.statics.acceptRequest = /*#__PURE__*/function () {
  var _acceptRequest = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee3(request_id) {
    return _regenerator["default"].wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            _context3.next = 2;
            return this.findOneAndUpdate({
              _id: request_id
            }, {
              $set: {
                status: 'A'
              }
            });

          case 2:
            return _context3.abrupt("return", this.findOne({
              _id: request_id
            }));

          case 3:
          case "end":
            return _context3.stop();
        }
      }
    }, _callee3, this);
  }));

  function acceptRequest(_x3) {
    return _acceptRequest.apply(this, arguments);
  }

  return acceptRequest;
}();

schema.statics.getIdsFriends = /*#__PURE__*/function () {
  var _getFriendsFrom = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee4(user) {
    var requestsAccepted, requestsDone;
    return _regenerator["default"].wrap(function _callee4$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            _context4.next = 2;
            return friendsModel.find({
              to: user,
              status: 'A'
            });

          case 2:
            requestsAccepted = _context4.sent;
            _context4.next = 5;
            return friendsModel.find({
              from: user,
              status: 'A'
            });

          case 5:
            requestsDone = _context4.sent;
            return _context4.abrupt("return", [].concat((0, _toConsumableArray2["default"])(requestsAccepted.map(function (request) {
              return request.from;
            })), (0, _toConsumableArray2["default"])(requestsDone.map(function (request) {
              return request.to;
            }))));

          case 7:
          case "end":
            return _context4.stop();
        }
      }
    }, _callee4);
  }));

  function getFriendsFrom(_x4) {
    return _getFriendsFrom.apply(this, arguments);
  }

  return getFriendsFrom;
}();

schema.statics.removeFriend = /*#__PURE__*/function () {
  var _removeFriend = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee5(friend_id, user_id) {
    return _regenerator["default"].wrap(function _callee5$(_context5) {
      while (1) {
        switch (_context5.prev = _context5.next) {
          case 0:
            _context5.next = 2;
            return this.deleteOne({
              $or: [{
                from: user_id,
                to: friend_id
              }, {
                to: user_id,
                from: friend_id
              }]
            });

          case 2:
            return _context5.abrupt("return", _context5.sent);

          case 3:
          case "end":
            return _context5.stop();
        }
      }
    }, _callee5, this);
  }));

  function removeFriend(_x5, _x6) {
    return _removeFriend.apply(this, arguments);
  }

  return removeFriend;
}();

schema.statics.getFriendsFrom = /*#__PURE__*/function () {
  var _getFriendsFrom2 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee6(user) {
    return _regenerator["default"].wrap(function _callee6$(_context6) {
      while (1) {
        switch (_context6.prev = _context6.next) {
          case 0:
            _context6.t0 = _User["default"];
            _context6.next = 3;
            return this.getIdsFriends(user);

          case 3:
            _context6.t1 = _context6.sent;
            _context6.t2 = {
              $in: _context6.t1
            };
            _context6.t3 = {
              _id: _context6.t2
            };
            return _context6.abrupt("return", _context6.t0.find.call(_context6.t0, _context6.t3));

          case 7:
          case "end":
            return _context6.stop();
        }
      }
    }, _callee6, this);
  }));

  function getFriendsFrom(_x7) {
    return _getFriendsFrom2.apply(this, arguments);
  }

  return getFriendsFrom;
}();

schema.statics.getOtherPeople = /*#__PURE__*/function () {
  var _getOtherPeople = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee7(user) {
    var requests, incomingRequests, friends, idsToExlude;
    return _regenerator["default"].wrap(function _callee7$(_context7) {
      while (1) {
        switch (_context7.prev = _context7.next) {
          case 0:
            _context7.next = 2;
            return this.getPendingRequestsFromUser(user);

          case 2:
            requests = _context7.sent;
            _context7.next = 5;
            return this.getPendingRequestsToUser(user);

          case 5:
            incomingRequests = _context7.sent;
            _context7.next = 8;
            return this.getIdsFriends(user);

          case 8:
            friends = _context7.sent;
            idsToExlude = [].concat((0, _toConsumableArray2["default"])(requests.map(function (_ref) {
              var to = _ref.to;
              return to;
            })), (0, _toConsumableArray2["default"])(incomingRequests.map(function (_ref2) {
              var from = _ref2.from;
              return from;
            })), (0, _toConsumableArray2["default"])(friends), [user]);
            _context7.next = 12;
            return _User["default"].find({
              _id: {
                $not: {
                  $in: idsToExlude
                }
              }
            });

          case 12:
            return _context7.abrupt("return", _context7.sent);

          case 13:
          case "end":
            return _context7.stop();
        }
      }
    }, _callee7, this);
  }));

  function getOtherPeople(_x8) {
    return _getOtherPeople.apply(this, arguments);
  }

  return getOtherPeople;
}();

var friendsModel = _mongoose["default"].model(collectionFriendsName, schema);

exports.friendsModel = friendsModel;
var _default = friendsModel;
exports["default"] = _default;