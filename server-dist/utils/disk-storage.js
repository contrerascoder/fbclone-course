"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = exports.getUploader = void 0;

var _multer = _interopRequireDefault(require("multer"));

var _path = _interopRequireDefault(require("path"));

var rootPath = _path["default"].join(__dirname, '../public');

var getStorage = function getStorage(dir) {
  return _multer["default"].diskStorage({
    destination: function destination(req, file, cb) {
      cb(null, _path["default"].join(rootPath, dir));
    },
    filename: function filename(req, file, cb) {
      cb(null, file.fieldname + '-' + Date.now());
    }
  });
};

var getUploader = function getUploader(dir) {
  return (0, _multer["default"])({
    storage: getStorage(dir)
  });
};

exports.getUploader = getUploader;
var _default = getUploader;
exports["default"] = _default;