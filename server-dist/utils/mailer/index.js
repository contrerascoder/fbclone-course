"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _mailgunJs = _interopRequireDefault(require("mailgun-js"));

var _vars = require("../../config/vars");

var _pug = _interopRequireDefault(require("pug"));

var _path = _interopRequireDefault(require("path"));

var mailjet = require('node-mailjet').connect(_vars.API_KEY_MJ, _vars.API_SECRET_MJ);

var getFile = function getFile(name) {
  return _path["default"].join(__dirname, 'templates', "".concat(name, ".pug"));
};

var Mailer = /*#__PURE__*/function () {
  function Mailer(to) {
    (0, _classCallCheck2["default"])(this, Mailer);
    this.to = to;
    this.renderers = {
      password: _pug["default"].compileFile(getFile('password')),
      verify: _pug["default"].compileFile(getFile('verify'))
    };
  }

  (0, _createClass2["default"])(Mailer, [{
    key: "sendMail",
    value: function () {
      var _sendMail = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee(subject, text) {
        var data, result;
        return _regenerator["default"].wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                data = {
                  "From": {
                    "Email": _vars.EMAIL_MJ,
                    "Name": "raul"
                  },
                  To: [{
                    Email: this.to,
                    name: 'john doe'
                  }],
                  Subject: subject,
                  HTMLPart: text
                };
                _context.prev = 1;
                _context.next = 4;
                return mailjet.post('send', {
                  version: 'v3.1'
                }).request({
                  Messages: [data]
                });

              case 4:
                result = _context.sent;
                return _context.abrupt("return", result.body);

              case 8:
                _context.prev = 8;
                _context.t0 = _context["catch"](1);
                console.log(_context.t0.message);

              case 11:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this, [[1, 8]]);
      }));

      function sendMail(_x, _x2) {
        return _sendMail.apply(this, arguments);
      }

      return sendMail;
    }()
  }, {
    key: "render",
    value: function render(renderer, data) {
      return this.renderers[renderer](data);
    }
  }]);
  return Mailer;
}();

exports["default"] = Mailer;