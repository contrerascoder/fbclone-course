"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = connectToDB;

var _mongoose = _interopRequireDefault(require("mongoose"));

var _vars = require("../config/vars");

function connectToDB(cb) {
  _mongoose["default"].connect(_vars.MONGODB_URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true
  }).then(function () {
    console.log('Se ha conectado correctamente a la base de datos: ' + _vars.MONGODB_URI);
    cb();
  })["catch"](function (err) {
    console.log('No se pudo conectar a la base de datos correctamente: ' + err.message);
    console.log('Información de la base de datos: ' + _vars.MONGODB_URI);
  });
}