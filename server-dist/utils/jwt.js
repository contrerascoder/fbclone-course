"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.tokenize = tokenize;
exports.verifyToken = verifyToken;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _jsonwebtoken = _interopRequireDefault(require("jsonwebtoken"));

var _vars = require("../config/vars");

var _User = _interopRequireDefault(require("../models/User"));

function tokenize(payload) {
  var token = _jsonwebtoken["default"].sign(payload, _vars.SECRET_KEY, {
    expiresIn: '2 days'
  });

  return token;
}

function verifyToken(req, res, next) {
  try {
    if (!req.headers.authorization) return next();

    _jsonwebtoken["default"].verify(req.headers.authorization, _vars.SECRET_KEY, /*#__PURE__*/function () {
      var _ref = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee(err, decoded) {
        var user;
        return _regenerator["default"].wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                if (!err) {
                  _context.next = 3;
                  break;
                }

                console.log('Hubo un error:' + err.message);
                return _context.abrupt("return", res.status(401).end(err.message));

              case 3:
                _context.next = 5;
                return _User["default"].findOne({
                  email: decoded.email
                });

              case 5:
                user = _context.sent;
                req.user = user;
                next();

              case 8:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }));

      return function (_x, _x2) {
        return _ref.apply(this, arguments);
      };
    }());
  } catch (error) {
    res.status(400).end('El token expxiro');
  }
}