"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.signInValidator = exports.signUpValidator = void 0;

var _toConsumableArray2 = _interopRequireDefault(require("@babel/runtime/helpers/toConsumableArray"));

var _user = require("./user");

var _require = require("express-validator"),
    validationResult = _require.validationResult;

var createValidators = function createValidators(validators) {
  return [].concat((0, _toConsumableArray2["default"])(validators), [function (req, res, next) {
    var errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res.status(422).json({
        errors: errors.array()
      });
    }

    next();
  }]);
};

var signUpValidator = createValidators([_user.nameIsPresent, _user.surnameIsPresent, _user.emailIsPresent, _user.emailFormatIsCorrect, _user.passwordIsPresent]);
exports.signUpValidator = signUpValidator;
var signInValidator = createValidators([_user.emailIsPresent, _user.emailFormatIsCorrect, _user.passwordIsPresent]);
exports.signInValidator = signInValidator;