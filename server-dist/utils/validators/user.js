"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.passwordIsPresent = exports.emailFormatIsCorrect = exports.emailIsPresent = exports.surnameIsPresent = exports.nameIsPresent = void 0;

var _expressValidator = require("express-validator");

var nameIsPresent = (0, _expressValidator.check)('name').notEmpty().withMessage('El nombre es obligatorio');
exports.nameIsPresent = nameIsPresent;
var surnameIsPresent = (0, _expressValidator.check)('surname').notEmpty().withMessage('Los apellidos son obligatorios');
exports.surnameIsPresent = surnameIsPresent;
var emailIsPresent = (0, _expressValidator.check)('email').notEmpty().withMessage('El email es obligatorio');
exports.emailIsPresent = emailIsPresent;
var emailFormatIsCorrect = (0, _expressValidator.check)('email').isEmail().withMessage('El formato del email no es correcto');
exports.emailFormatIsCorrect = emailFormatIsCorrect;
var passwordIsPresent = (0, _expressValidator.check)('password').notEmpty().withMessage('La contraseña es obligatoria');
exports.passwordIsPresent = passwordIsPresent;