"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = checkIsLogged;

function checkIsLogged(req, res, next) {
  if (!req.headers.authorization) {
    return res.status(400).end('Tienes que estar logueado para hacer esta acción');
  }

  next();
}