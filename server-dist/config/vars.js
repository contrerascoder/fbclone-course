"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.APP_BASE_FILES_ADDRESS = exports.APP_BASE_URL = exports.EMAIL_MJ = exports.API_SECRET_MJ = exports.API_KEY_MJ = exports.SALTS = exports.SECRET_KEY = exports.MONGODB_URI = exports.SERVER_PORT = void 0;
var SERVER_PORT = process.env.PORT || '8080';
exports.SERVER_PORT = SERVER_PORT;
var MONGODB_URI = process.env.MONGODB_URI || 'mongodb://localhost/fbclone';
exports.MONGODB_URI = MONGODB_URI;
var SECRET_KEY = process.env.SECRET_KEY || 'msdasqwklscklmklmlksdsakklsxksx';
exports.SECRET_KEY = SECRET_KEY;
var SALTS = Number(process.env.SALTS) || 10;
exports.SALTS = SALTS;
var API_KEY_MJ = process.env.API_KEY_MJ;
exports.API_KEY_MJ = API_KEY_MJ;
var API_SECRET_MJ = process.env.API_SECRET_MJ;
exports.API_SECRET_MJ = API_SECRET_MJ;
var EMAIL_MJ = process.env.EMAIL_MJ;
exports.EMAIL_MJ = EMAIL_MJ;
var APP_BASE_URL = process.env.APP_BASE_URL;
exports.APP_BASE_URL = APP_BASE_URL;
var APP_BASE_FILES_ADDRESS = process.env.APP_BASE_FILES_ADDRESS || "/";
exports.APP_BASE_FILES_ADDRESS = APP_BASE_FILES_ADDRESS;