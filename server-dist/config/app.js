"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = exports.app = void 0;

var _express = _interopRequireDefault(require("express"));

var _routes = _interopRequireDefault(require("../routes"));

var _jwt = require("../utils/jwt");

var _path = _interopRequireDefault(require("path"));

var app = (0, _express["default"])();
exports.app = app;
app.use(_express["default"]["static"](_path["default"].join(__dirname, '..', 'public/')));

if (process.env.NODE_ENV !== 'production') {
  app.use(require('cors')());
}

app.use(_express["default"].json());
app.use(_jwt.verifyToken);
app.use('/api', _routes["default"]);
app.get('/test', function (req, res) {
  console.log('peticion recibida');
  res.end('He recibido tu peticion');
});
var _default = app;
exports["default"] = _default;