"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _toConsumableArray2 = _interopRequireDefault(require("@babel/runtime/helpers/toConsumableArray"));

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _require = require('../utils/connect-db'),
    connectToDB = _require["default"];

var _require2 = require('../models/User'),
    UserModel = _require2["default"];

var _require3 = require('../models/Friend'),
    friendsModel = _require3["default"];

require('dotenv/config');

var userId = '5f3ab0c842af093fe4439942';
connectToDB( /*#__PURE__*/(0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee() {
  return _regenerator["default"].wrap(function _callee$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          console.log('conectado a la base de datos');
          /*
              const friends = await friendsModel.getFriendsFrom(userId)
              const ids = [
                  ...friends.requestsAccepted.map(request => request.from),
                  ...friends.requestsDone.map(request => request.to),
              ]
          
              console.log(friends);
          
              console.log(ids);*/

          _context.t0 = console;
          _context.next = 4;
          return friendsModel.getFriendsFrom(userId);

        case 4:
          _context.t1 = _context.sent;

          _context.t0.log.call(_context.t0, _context.t1);

          process.exit();

        case 7:
        case "end":
          return _context.stop();
      }
    }
  }, _callee);
}))); // Conseguir todas las peticiones de un usuario

function allPendingRequestsFromUser(_x) {
  return _allPendingRequestsFromUser.apply(this, arguments);
} // Conseguir todas las peticiones a un usuario


function _allPendingRequestsFromUser() {
  _allPendingRequestsFromUser = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee2(user) {
    return _regenerator["default"].wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            _context2.next = 2;
            return friendsModel.find({
              from: user
            });

          case 2:
            return _context2.abrupt("return", _context2.sent);

          case 3:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2);
  }));
  return _allPendingRequestsFromUser.apply(this, arguments);
}

function allPendingRequestsToUser(_x2) {
  return _allPendingRequestsToUser.apply(this, arguments);
}

function _allPendingRequestsToUser() {
  _allPendingRequestsToUser = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee3(user) {
    return _regenerator["default"].wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            _context3.next = 2;
            return friendsModel.find({
              to: user
            });

          case 2:
            return _context3.abrupt("return", _context3.sent);

          case 3:
          case "end":
            return _context3.stop();
        }
      }
    }, _callee3);
  }));
  return _allPendingRequestsToUser.apply(this, arguments);
}

function allOtherPeople(_x3) {
  return _allOtherPeople.apply(this, arguments);
}

function _allOtherPeople() {
  _allOtherPeople = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee4(user) {
    var requests, incomingRequests, idsToExlude;
    return _regenerator["default"].wrap(function _callee4$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            _context4.next = 2;
            return allPendingRequestsFromUser(userId);

          case 2:
            requests = _context4.sent;
            _context4.next = 5;
            return allPendingRequestsToUser(userId);

          case 5:
            incomingRequests = _context4.sent;
            idsToExlude = [].concat((0, _toConsumableArray2["default"])(requests.map(function (_ref2) {
              var to = _ref2.to;
              return to;
            })), (0, _toConsumableArray2["default"])(incomingRequests.map(function (_ref3) {
              var from = _ref3.from;
              return from;
            })), [// Usuarios que te han hecho la petición
            user]);
            _context4.next = 9;
            return UserModel.find({
              _id: {
                $not: {
                  $in: idsToExlude
                }
              }
            });

          case 9:
            return _context4.abrupt("return", _context4.sent);

          case 10:
          case "end":
            return _context4.stop();
        }
      }
    }, _callee4);
  }));
  return _allOtherPeople.apply(this, arguments);
}