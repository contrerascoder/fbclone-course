"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

require("dotenv/config");

var _http = _interopRequireDefault(require("http"));

var _vars = require("../config/vars");

var _connectDb = _interopRequireDefault(require("../utils/connect-db"));

(0, _connectDb["default"])(function () {
  try {
    var app = require('../config/app')["default"];

    var server = _http["default"].createServer(app);

    server.listen(_vars.SERVER_PORT, function () {
      console.log('Escuchando por el puerto ' + _vars.SERVER_PORT);
    });
  } catch (error) {
    console.log(error.message);
    console.log(error);
  }
});