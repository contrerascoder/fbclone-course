import mongoose from 'mongoose'
import { hashPassword } from '../utils/crypt'
import uniqueString from 'unique-string'

export const collectionUsersName = 'users'

const schema = new mongoose.Schema({
    name: {type: String, required: true},
    surname: {type: String, required: true},
    email: {type: String, required: true},
    password: {type: String, required: true},
    secret: {type: String, default: uniqueString},
    verified: {type: Boolean, default: false},
    avatar: {type: String, default: ''}
})

schema.pre('save', async function(next) {
    console.log('encriptando contraseña', this.password);
    this.password = await hashPassword(this.password)
    next()
})

export const UserModel = mongoose.model(collectionUsersName, schema)

export default UserModel
