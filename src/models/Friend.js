import mongoose from 'mongoose'
import UserModel, { collectionUsersName } from './User'
import { request } from 'express'

export const collectionFriendsName = 'friends'

const schema = new mongoose.Schema({
    from: {type: mongoose.Types.ObjectId, ref: collectionUsersName},
    to: {type: mongoose.Types.ObjectId, ref: collectionUsersName},
    status: {type: String, enum: ['P', 'A'], default: 'P'},
})

schema.statics.createRequest = function sendRequest(from, to) {
    return this.create({from, to})
}

schema.statics.getPendingRequestsFromUser = async function getPendingRequestsFromUser(user) {
    return await this.find({from: user, status: 'P'})
}

schema.statics.getPendingRequestsToUser = async function getPendingRequestsToUser(user) {
    return await this.find({to: user, status: 'P'}).populate('from', ['name', 'surname'])
}

schema.statics.acceptRequest = async function acceptRequest(request_id) {
    await this.findOneAndUpdate({_id: request_id}, {$set: {status: 'A'}})
    return this.findOne({_id: request_id})
}

schema.statics.getIdsFriends = async function getFriendsFrom(user) {
    const requestsAccepted = await friendsModel.find({to: user, status: 'A'}) // peticiones al usuario
    const requestsDone = await friendsModel.find({from: user, status: 'A'}) // peticiones del usuario
    return [
        ...requestsAccepted.map(request => request.from),
        ...requestsDone.map(request => request.to),
    ]
}

schema.statics.removeFriend = async function removeFriend(friend_id, user_id) {
    return await this.deleteOne({
        $or: [
            {from: user_id, to: friend_id},
            {to: user_id, from: friend_id},
        ]
    })
}

schema.statics.getFriendsFrom = async function getFriendsFrom(user) {
    return UserModel.find({_id: {$in: await this.getIdsFriends(user)}})
}

schema.statics.getOtherPeople = async function getOtherPeople(user) {
    const requests = await this.getPendingRequestsFromUser(user)
    const incomingRequests = await this.getPendingRequestsToUser(user)
    const friends = await this.getIdsFriends(user)

    const idsToExlude = [
        ...requests.map(({to}) => to), // Usuarios a los que les has hecho la petición
        ...incomingRequests.map(({from}) => from), // Usuarios que te han hecho la petición
        ...friends,
        user
    ]

    return await UserModel.find({_id: {$not: {$in: idsToExlude}}})
}

export const friendsModel = mongoose.model(collectionFriendsName, schema)

export default friendsModel
