import mongoose from 'mongoose'
import { collectionUsersName } from './User'
import { collectionPostsName } from './Post'

export const collectionCommentName = 'comments'

const schema = new mongoose.Schema({
    text: {type: String, default: ''},
    date: {type: Number, default: Date.now},
    user: {type: mongoose.Types.ObjectId, ref: collectionUsersName},
    post: {type: mongoose.Types.ObjectId, ref: collectionPostsName},
    childOf: {type: mongoose.Types.ObjectId, ref: collectionCommentName, default: null},
})

export const CommentsModel = mongoose.model(collectionCommentName, schema)

export default CommentsModel
