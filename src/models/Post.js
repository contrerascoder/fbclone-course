import mongoose from 'mongoose'
import { hashPassword } from '../utils/crypt'
import { collectionUsersName } from './User'

export const collectionPostsName = 'posts'

const schema = new mongoose.Schema({
    text: {type: String, default: ''},
    date: {type: Number, default: Date.now},
    user: {type: mongoose.Types.ObjectId, ref: collectionUsersName},
    image: {type: String, default: ''}
})

export const PostsModel = mongoose.model(collectionPostsName, schema)

export default PostsModel
