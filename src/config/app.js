import express from 'express'
import router from '../routes'
import { verifyToken } from '../utils/jwt'
import path from 'path'

export const app = express()

app.use(express.static(path.join(__dirname, '..', 'public/')))
if (process.env.NODE_ENV !== 'production') {
    app.use(require('cors')())
}
app.use(express.json())
app.use(verifyToken)

app.use('/api', router)

app.get('/test', (req, res) => {
    console.log('peticion recibida');
    res.end('He recibido tu peticion')
})

export default app
