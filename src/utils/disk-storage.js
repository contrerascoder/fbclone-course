import multer from 'multer'
import path from 'path'

const rootPath = path.join(__dirname, '../public')

const getStorage = dir => multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, path.join(rootPath, dir))
    },
    filename: function (req, file, cb) {
        cb(null, file.fieldname + '-' + Date.now())
    }
})

export const getUploader = dir => multer({ storage: getStorage(dir) })

export default getUploader
