import mongoose from 'mongoose'
import { MONGODB_URI } from '../config/vars'

export default function connectToDB(cb) {
    mongoose.connect(MONGODB_URI, {
        useNewUrlParser: true,
        useUnifiedTopology: true
    }).then(() => {
        console.log('Se ha conectado correctamente a la base de datos: ' + MONGODB_URI)
        cb()
    }).catch(err => {
        console.log('No se pudo conectar a la base de datos correctamente: ' + err.message)
        console.log('Información de la base de datos: ' + MONGODB_URI);
    })
}