export default function checkIsLogged(req, res, next) {
    if (!req.headers.authorization) {
        return res.status(400).end('Tienes que estar logueado para hacer esta acción')
    }
    next()
}
