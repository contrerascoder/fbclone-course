import jwt from 'jsonwebtoken'
import { SECRET_KEY } from '../config/vars'
import UserModel from '../models/User';

export function tokenize(payload) {
    const token = jwt.sign(payload, SECRET_KEY, {expiresIn: '2 days'})
    return token
}

export function verifyToken(req, res, next) {
    try {
        if (!req.headers.authorization) return next()
        jwt.verify(req.headers.authorization, SECRET_KEY, async (err, decoded) => {
            if (err) {
                console.log('Hubo un error:' + err.message);
                return res.status(401).end(err.message)
            }
            const user = await UserModel.findOne({email: decoded.email})
            req.user = user
            next()
        })
    } catch (error) {
        res.status(400).end('El token expxiro')
    }
}
