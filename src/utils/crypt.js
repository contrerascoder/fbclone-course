import bcrypt from 'bcrypt'
import { SALTS } from '../config/vars'

export async function hashPassword(input) {
    const salts = await bcrypt.genSalt(SALTS)
    return await bcrypt.hash(input, salts)
}

export async function checkPassword(input, hash) {
    return await bcrypt.compare(input, hash)
}
