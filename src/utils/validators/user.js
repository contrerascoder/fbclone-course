import { check } from "express-validator";

export const nameIsPresent = check('name').notEmpty().withMessage('El nombre es obligatorio')
export const surnameIsPresent = check('surname').notEmpty().withMessage('Los apellidos son obligatorios')
export const emailIsPresent = check('email').notEmpty().withMessage('El email es obligatorio')
export const emailFormatIsCorrect = check('email').isEmail().withMessage('El formato del email no es correcto')
export const passwordIsPresent = check('password').notEmpty().withMessage('La contraseña es obligatoria')
