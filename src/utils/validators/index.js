import { nameIsPresent, surnameIsPresent, emailIsPresent, passwordIsPresent, emailFormatIsCorrect } from "./user";

const { validationResult } = require("express-validator")

const createValidators = (validators) => ([...validators, (req, res, next) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res.status(422).json({errors: errors.array()})
    }
    next()
}])

export const signUpValidator = createValidators([nameIsPresent, surnameIsPresent, emailIsPresent, emailFormatIsCorrect, passwordIsPresent])
export const signInValidator = createValidators([emailIsPresent, emailFormatIsCorrect, passwordIsPresent])
