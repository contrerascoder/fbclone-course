import mg from 'mailgun-js'
import { API_KEY_MJ, API_SECRET_MJ, EMAIL_MJ } from '../../config/vars'
import pug from 'pug'
import path from 'path'
const mailjet = require ('node-mailjet').connect(API_KEY_MJ, API_SECRET_MJ)

const getFile = name => path.join(__dirname, 'templates', `${name}.pug`)

export default class Mailer {
    constructor(to) {
        this.to = to
        this.renderers = {
            password: pug.compileFile(getFile('password')),
            verify: pug.compileFile(getFile('verify')),
        }
    }

    async sendMail(subject, text) {
        const data = {
            "From": {
                "Email": EMAIL_MJ,
                "Name": "raul"
            },
            To: [{
                Email: this.to,
                name: 'john doe'
            }],
            Subject: subject,
            HTMLPart: text
        };

        try {
            const result = await mailjet.post('send', {version: 'v3.1'}).request({Messages: [data]})
            return result.body
        } catch (error) {
            console.log(error.message);
        }
    }

    render(renderer, data) {
        return this.renderers[renderer](data)
    }
}
