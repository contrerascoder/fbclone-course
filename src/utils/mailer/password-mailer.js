import Mailer from ".";

export default class PasswordMailer extends Mailer {
    async buildAndSend(data) {
        const html = this.render('password', data)
        console.log('enviando...');
        await this.sendMail('Recuperación de contraseña', html)
    }
}
