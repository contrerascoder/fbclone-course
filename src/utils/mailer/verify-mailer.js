import Mailer from ".";

export default class VerifyMailer extends Mailer {
    async buildAndSend(data) {
        const html = this.render('verify', data)
        console.log('enviando...');
        await this.sendMail('Verificación de correo', html)
    }
}
