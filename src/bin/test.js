const { default: connectToDB } = require('../utils/connect-db');
const { default: UserModel } = require('../models/User');
const { default: friendsModel } = require('../models/Friend');

require('dotenv/config')

const userId = '5f3ab0c842af093fe4439942'

connectToDB(async () => {
    console.log('conectado a la base de datos');
/*
    const friends = await friendsModel.getFriendsFrom(userId)
    const ids = [
        ...friends.requestsAccepted.map(request => request.from),
        ...friends.requestsDone.map(request => request.to),
    ]

    console.log(friends);

    console.log(ids);*/

    console.log(await friendsModel.getFriendsFrom(userId))

    process.exit()
})

// Conseguir todas las peticiones de un usuario
async function allPendingRequestsFromUser(user) {
    return await friendsModel.find({from: user})
}
// Conseguir todas las peticiones a un usuario
async function allPendingRequestsToUser(user) {
    return await friendsModel.find({to: user})
}

async function allOtherPeople(user) {
    const requests = await allPendingRequestsFromUser(userId)
    const incomingRequests = await allPendingRequestsToUser(userId)

    const idsToExlude = [
        ...requests.map(({to}) => to), // Usuarios a los que les has hecho la petición
        ...incomingRequests.map(({from}) => from), // Usuarios que te han hecho la petición
        user
    ]

    return await UserModel.find({_id: {$not: {$in: idsToExlude}}})
}
