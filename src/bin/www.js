import 'dotenv/config'
import http from 'http'
import { SERVER_PORT } from '../config/vars'
import connectToDB from '../utils/connect-db';

connectToDB(() => {
    try {
        const app = require('../config/app').default

        const server = http.createServer(app)
        server.listen(SERVER_PORT, () => {
            console.log('Escuchando por el puerto ' + SERVER_PORT);
        })
    } catch (error) {
        console.log(error.message);
        console.log(error);
    }
})

