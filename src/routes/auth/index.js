import express from 'express'
import authController from './AuthController'
import { signUpValidator, signInValidator } from '../../utils/validators'
import getUploader from '../../utils/disk-storage'

const uploader = getUploader('img/avatars')

export const authRouter = express.Router()

authRouter.post('/signup', signUpValidator, authController.signup)
authRouter.post('/signin', signInValidator, authController.signin)
authRouter.get('/passlink', authController.sendPasswordLink)
authRouter.put('/password/:secret', authController.changePassword)
authRouter.put('/verify/:secret', authController.activate)
authRouter.put('/avatar', uploader.single('avatar'), authController.updateAvatar)

export default authRouter
