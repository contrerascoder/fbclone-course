import UserModel from "../../models/User"
import { tokenize } from "../../utils/jwt"
import { checkPassword, hashPassword } from "../../utils/crypt"
import uniqueString from 'unique-string'
import { APP_BASE_URL, APP_BASE_FILES_ADDRESS } from "../../config/vars"
import PasswordMailer from "../../utils/mailer/password-mailer"
import VerifyMailer from "../../utils/mailer/verify-mailer"
import fsExtra from 'fs-extra'
import path from 'path'

class AuthController {
    async signup(req, res) {
        const user = await UserModel.findOne({email: req.body.email})
        if (user) {
            return res.status(400).end('El usuario existe')
        }
        const userCreated = await UserModel.create(req.body)
        const mailer = new VerifyMailer(userCreated.email)
        await mailer.buildAndSend({
            link: `${APP_BASE_URL}/auth/users/${userCreated.secret}/activate`,
        })
        res.status(201).json(userCreated)
    }

    async signin(req, res) {
        const user = await UserModel.findOne({email: req.body.email, verified: true})
        if (!user) {
            return res.status(400).end('Las credenciales son incorrectas')
        }
        if ( !(await checkPassword(req.body.password, user.password)) ) {
            return res.status(400).end('Las credenciales son incorrectas')
        }

        const tokenData = {
            name: user.name,
            email: user.email,
            avatar: user.avatar
        }

        const token = tokenize(tokenData)

        return res.status(201).json({token: token, userData: tokenData})
    }

    async sendPasswordLink(req, res) {
        // const secret = await updateAndGetUniqueString(req.query.email)
        const user = await UserModel.findOne({email: req.query.email})
        console.log(user);
        const mailer = new PasswordMailer(user.email)
        await mailer.buildAndSend({
            link: `${APP_BASE_URL}/auth/users/${user.secret}/pass`,
            fullname: 'raul contreras'
        })
        res.status(200).end('email enviado')
    }

    async changePassword(req, res) {
        const newPass = await hashPassword(req.body.password)
        console.log(newPass);
        await UserModel.updateOne({secret: req.params.secret}, {
            $set: {
                password: newPass
            }
        })
        await updateAndGetUniqueString()
        res.status(200).end('Contraseña cambiada')
    }

    async activate (req, res) {
        await UserModel.findOneAndUpdate({secret: req.params.secret}, {
            $set: {
                verified: true
            }
        })
        res.status(200).end('Cuenta activada')
    }

    async updateAvatar(req, res) {
        const newName = req.user._id + ".png"
        await fsExtra.rename(req.file.path, path.join(req.file.destination, newName))
        const addressAvatar = APP_BASE_FILES_ADDRESS + "/img/avatars/" + newName
        UserModel.updateOne({_id: req.user._id}, {
            $set: {avatar: addressAvatar}
        })
        res.status(200).end(addressAvatar)
    }
}
async function updateAndGetUniqueString(email) {
    const newSecret = uniqueString()
    const data = {
        $set: {secret: newSecret}
    }
    await UserModel.findOneAndUpdate({email: email}, {$set: data})
    return newSecret
}

export const authController = new AuthController()

export default authController
