import express from 'express'
import authRouter from './auth'
import postsRouter from './posts'
import commentsRouter from './comments'
import friendsRouter from './friends'

export const router = new express.Router()

router.use('/auth', authRouter)
router.use('/posts', postsRouter)
router.use('/comments', commentsRouter)
router.use('/friends', friendsRouter)

export default router
