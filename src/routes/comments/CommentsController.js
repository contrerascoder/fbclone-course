import CommentsModel from "../../models/Comment"
import PostsModel from "../../models/Post"

class CommentController {
    async create(req, res) {
        const comment = await CommentsModel.create({
            text: req.body.message,
            user: req.user,
            post: await PostsModel.findOne({_id: req.params.postId})
        })
        return res.status(201).json(comment)
    }
    async getAll(req, res) {
        const comments = await CommentsModel.find({post: req.params.postId, childOf: null}).sort('-date').populate('user', ['name', 'surname'])
        return res.status(200).json(comments)
    }
    async respondToComment(req, res) {
        console.log(req.params.commentId);
        const comment = await CommentsModel.create({
            text: req.body.message,
            user: req.user,
            // post: await PostsModel.findOne({_id: req.params.postId}),
            childOf: req.params.commentId
        })
        return res.status(201).json(comment)
    }
    async getResponses(req, res) {
        const comments = await CommentsModel.find({childOf: req.params.commentId}).sort('-date').populate('user', ['name', 'surname'])
        return res.status(200).json(comments)
    }
}

export const commentController = new CommentController()

export default commentController
