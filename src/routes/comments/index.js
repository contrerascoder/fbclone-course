import express from 'express'
import commentController from './CommentsController'
import checkIsLogged from '../../utils/is-logged'

const commentsRouter = express.Router()

commentsRouter.post('/post/:postId', checkIsLogged, commentController.create)
commentsRouter.get('/post/:postId', commentController.getAll)
commentsRouter.post('/respond/:commentId', checkIsLogged, commentController.respondToComment)
commentsRouter.get('/responses/:commentId', commentController.getResponses)

export default commentsRouter
