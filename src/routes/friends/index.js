import express from 'express'
import friendsController from './FriendsController'

const friendsRouter = express.Router()

// Ruta para obtener todas las personas
friendsRouter.get('/', friendsController.getAll)
// Ruta para mandar peticiones
friendsRouter.post('/request/:id', friendsController.sendRequest)
// Ruta para obtener las peticiones
friendsRouter.get('/requests', friendsController.getRequests)
// Ruta para aceptar peticiones
friendsRouter.put('/accept/:id', friendsController.acceptRequest)
// Ruta para obtener todos los amigos
friendsRouter.get('/friends', friendsController.getFriends)
// Ruta para eliminar amigos
friendsRouter.delete('/remove/:id', friendsController.removeFriend)


export default friendsRouter
