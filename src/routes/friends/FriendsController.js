import friendsModel from "../../models/Friend"
import UserModel from "../../models/User"

class FriendsController {
    async getAll(req, res) {
        const people = await friendsModel.getOtherPeople(req.user._id)
        res.status(200).json({people})
    }
    async sendRequest(req, res) {
        await friendsModel.createRequest(req.user._id, req.params.id)
        res.status(201).end('Petición mandada')
    }
    async getRequests(req, res) {
        const requests = await friendsModel.getPendingRequestsToUser(req.user._id)
        res.status(200).json({requests})
    }
    async acceptRequest(req, res) {
        await friendsModel.acceptRequest(req.params.id)
        res.status(200).end('Petición aceptada')
    }
    async getFriends(req, res) {
        const friends = await friendsModel.getFriendsFrom(req.user._id)
        res.status(200).json({friends})
    }
    async removeFriend(req, res) {
        await friendsModel.removeFriend(req.params.id, req.user._id)
        res.status(200).end('Amigo eliminado')
    }
}

export const friendsController = new FriendsController()

export default friendsController
