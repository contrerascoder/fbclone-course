import express from 'express'
import postController from './PostController'
import checkIsLogged from '../../utils/is-logged'
import getUploader from '../../utils/disk-storage'

const uploader = getUploader('img/posts')
const postsRouter = express.Router()

postsRouter.post('/', checkIsLogged, uploader.any(), postController.create)
postsRouter.get('/', postController.getAll)
postsRouter.put('/:postId', checkIsLogged, postController.update)
postsRouter.delete('/:postId', checkIsLogged, postController.delete)

export default postsRouter
