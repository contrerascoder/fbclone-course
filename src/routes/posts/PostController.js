const { default: PostsModel } = require("../../models/Post");
import fsExtra from 'fs-extra'
import { APP_BASE_FILES_ADDRESS } from '../../config/vars';

class PostController {
    async create(req, res) {
        const dataPost = {
            text: req.body.message,
            user: req.user
        }
        const image = req.files && req.files[0]
        if (image) {
            await fsExtra.rename(image.path, image.path + ".png")
            dataPost.image = APP_BASE_FILES_ADDRESS + "/img/posts/" + image.filename + ".png"
        }
        const post = await PostsModel.create(dataPost)
        return res.status(201).json(post)
    }
    async getAll(req, res) {
        const posts = await PostsModel.find({}).sort('-date').populate('user', ['name', 'surname'])
        return res.status(200).json(posts)
    }
    async update(req, res) {
        const update = {text: req.body.message}
        await PostsModel.updateOne({_id: req.params.postId}, {$set: update})
        return res.status(200).json({
            post: await PostsModel.findOne({_id: req.params.postId}).populate('user', ['name', 'surname'])
        })
    }
    async delete(req, res) {
        const post = await PostsModel.deleteOne({_id: req.params.postId})
        console.log(req.params.postId, post);
        return res.status(200).end('Se elimino correctamente')
    }
}

export const postController = new PostController()

export default postController
