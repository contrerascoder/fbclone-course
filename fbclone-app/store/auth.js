export const state = () => ({
    token: null,
    userData: null
})

export const mutations = {
    setInfo(state, {token, userData}) {
        state.token = token
        state.userData = userData
    },
    setAvatar(state, avatar) {
        state.userData.avatar = avatar
    },
    trashSesion(state) {
        state.token = null
        state.userData = null
    }
}

export const getters = {
    isLogged(state) {
        return state.userData !== null
    }
}
