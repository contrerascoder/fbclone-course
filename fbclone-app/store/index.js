export const state = () => ({
    ready: false
})

export const mutations = {
    setReady(state, value) {
        state.ready = value
    }
}
