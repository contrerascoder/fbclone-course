import createPersistedState from 'vuex-persistedstate'
import Vue from 'vue'
import { mapState, mapGetters } from "vuex";

Vue.mixin({
    computed: {
        ...mapState('auth', ['token', 'userData']),
        ...mapGetters('auth', ['isLogged'])
    }
})

export default ({store, $axios}) => {
    createPersistedState({
        key: 'fbclone',
        paths: ['auth']
    })(store)
    $axios.setToken(store.state.auth.token)
    store.commit('setReady', true)


    $axios.interceptors.response.use(function (response) {
        // Do something with response data
        return response;
      }, function (error) {
          if (error.response.status === 401) {
              store.commit('auth/trashSesion')
              window.location.reload()
          }
        // Do something with response error
        return Promise.reject(error);
      });
}
