import Vue from 'vue'
import FBComment from '../components/fb-comment.vue'
import FBPost from '../components/fb-post.vue'
import FBUser from '../components/fb-user.vue'
import FBFriend from '../components/fb-friend.vue'
import FBRequest from '../components/fb-request.vue'
import UISubmitBox from '../components/ui-submitbox.vue'
import UITextarea from '../components/ui-textarea.vue'
import UITextinput from '../components/ui-textinput.vue'

Vue.component('fb-comment', FBComment)
Vue.component('fb-post', FBPost)
Vue.component('fb-user', FBUser)
Vue.component('fb-friend', FBFriend)
Vue.component('fb-request', FBRequest)
Vue.component('ui-submitbox', UISubmitBox)
Vue.component('ui-textarea', UITextarea)
Vue.component('ui-textinput', UITextinput)
